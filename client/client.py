import requests

base_url = 'http://localhost:8080'

import requests
import argparse

url = 'http://localhost:8080'


#initialisation des arguments
parser = argparse.ArgumentParser()
parser.add_argument('--query')
parser.add_argument('--player', action='store_true')
parser.add_argument('--team', action='store_true')
parser.add_argument('--id')
parser.add_argument('--name')
parser.add_argument('--firstname')
parser.add_argument('--team_name')
parser.add_argument('--position')
parser.add_argument('--city')
args = parser.parse_args()


if args.team:  #requétes equipes
    match args.query:
        case 'add': #methode post
            url = 'http://localhost:8080/team'
            new_team = {
                'id': args.id,
                'name': args.name,
                'city': args.city
            }
            response = requests.post(url, json=new_team)
            if response.status_code == 200:
                print('Création ok')
            else:
                print(f'Erreur lors de la création de lequipe. Code d\'erreur : {response.status_code}')
        case 'del':  #methode delete
           url = 'http://localhost:8080/team/'+args.id
           print(url)
           response = requests.delete(url)
           if response.status_code == 200:
                print('Joueur supprimé avec succès')
           else:
                print(f'Erreur lors de la suppression de lequipe. Code d\'erreur : {response.status_code}')

        case 'update':#methode put
            url = 'http://localhost:8080/team/'+args.id
            update_team = {
                'id': args.id,
                'name': args.name,
                'city': args.city
            }
            response = requests.put(url, json=update_team)
            if response.status_code == 200:
                print('update ok')
            else:
                print(f'Erreur lors de la mise à jour de lequipe. Code d\'erreur : {response.status_code}')

        case 'show':#methode get
            url = 'http://localhost:8080/team'
            response = requests.get(url)
            if response.status_code == 200:
                teams = response.json()
                for team in teams:
                    print(team)     
            else:
                print(f'Erreur lors de la récupération des équipes. Code d\'erreur : {response.status_code}')           

if args.player:#requétes joueurs
    match args.query:
        case 'add':#methode post
            url = 'http://localhost:8080/player'
            new_player = {
                'id': args.id,
                'name': args.name,
                'firstname': args.firstname,
                'poste': args.position,
                'team': args.team_name

            }
            response = requests.post(url, json=new_player)
            if response.status_code == 200:
                created_player = response.json()
                print('Création ok')
            else:
                print(f'Erreur lors de la création du joueur. Code d\'erreur : {response.status_code}')
        case 'del':#methode delete
           url = 'http://localhost:8080/player/'+args.id
           print(url)
           response = requests.delete(url)
           if response.status_code == 200:
                print('Joueur supprimé avec succès')
           else:
                print(f'Erreur lors de la suppression du joueur. Code d\'erreur : {response.status_code}')

        case 'update':#methode update
            url = 'http://localhost:8080/player/'+args.id
            update_player = {
                'id': args.id,
                'name': args.name,
                'firstname': args.firstname,
                'poste': args.position,
                'team': args.team_name
            }
            response = requests.put(url, json=update_player)
            if response.status_code == 200:
                print('update ok')
            else:
                print(f'Erreur lors de la mise à jour du joueur. Code d\'erreur : {response.status_code}')

        case 'show':#methode get
            url = 'http://localhost:8080/player'
            response = requests.get(url)
            if response.status_code == 200:
                players = response.json()
                for player in players:
                    print(player)     
            else:
                print(f'Erreur lors de la récupération des joueurs. Code d\'erreur : {response.status_code}')