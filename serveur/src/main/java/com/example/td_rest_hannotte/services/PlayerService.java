package com.example.td_rest_hannotte.services;
import java.util.List;
import com.example.td_rest_hannotte.domain.Player;

public interface PlayerService {

	public List<Player> findAllPlayer();	
	public Player createPlayer(Player Player);
	public void deletePlayer(Long id);
	public Player findPlayer(Long id);
	public Player savePlayer(Player foundPlayer);	
	
}
