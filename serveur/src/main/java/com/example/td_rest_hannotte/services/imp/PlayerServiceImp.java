package com.example.td_rest_hannotte.services.imp;
import com.example.td_rest_hannotte.services.PlayerService;
import com.example.td_rest_hannotte.domain.Player;
import com.example.td_rest_hannotte.dao.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PlayerServiceImp implements PlayerService {

	@Autowired
	private PlayerRepository player;
	
	
	@Transactional(readOnly = true)
	public List<Player> findAllPlayer() {
		
		return this.player.findAll();
	}	

	public Player createPlayer(Player player) {
		
		return this.player.save(player);
	}	
	
	@Override
	public void deletePlayer(Long id) {
		
		this.player.deleteById(id);
	}

	@Override	
	public Player savePlayer(Player foundPlayer) {
		
		return this.player.save(foundPlayer);
	}	

	@Override
	@Transactional(readOnly = true)
	public Player findPlayer(Long id) {

		return this.player.findById(id).orElse(null);
	}	
	
}
