package com.example.td_rest_hannotte.services.imp;
import com.example.td_rest_hannotte.services.*;
import com.example.td_rest_hannotte.domain.Team;
import com.example.td_rest_hannotte.dao.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class TeamServiceImp implements TeamService {

	@Autowired
	private TeamRepository team;
	
	
	@Transactional(readOnly = true)
	public List<Team> findAllteam() {
		
		return this.team.findAll();
	}	

	public Team createTeam(Team team) {
		
		return this.team.save(team);
	}	
	
	@Override
	public void deleteTeam(Long id) {
		
		this.team.deleteById(id);
	}

	@Override
	public Team saveTeam(Team foundTeam) {
		
		return this.team.save(foundTeam);
	}	

	@Override
	@Transactional(readOnly = true)
	public Team findTeam(Long id) {

		return this.team.findById(id).orElse(null);
	}	
	
}
