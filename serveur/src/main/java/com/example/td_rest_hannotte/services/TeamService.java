
package com.example.td_rest_hannotte.services;
import java.util.List;
import com.example.td_rest_hannotte.domain.Team;
public interface TeamService {
	
	public List<Team> findAllteam();	
	public Team createTeam(Team team);
	public void deleteTeam(Long id);
	public Team findTeam(Long id);
	public Team saveTeam(Team foundTeam);
}
