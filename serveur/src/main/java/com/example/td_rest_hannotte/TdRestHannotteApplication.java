package com.example.td_rest_hannotte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdRestHannotteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TdRestHannotteApplication.class, args);
	}

}
