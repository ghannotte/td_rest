package com.example.td_rest_hannotte.dao;
import com.example.td_rest_hannotte.domain.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {

}
