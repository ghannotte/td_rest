package com.example.td_rest_hannotte.dao;
import com.example.td_rest_hannotte.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}