
package com.example.td_rest_hannotte.controller;


import com.example.td_rest_hannotte.domain.*;
import com.example.td_rest_hannotte.services.TeamService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
public class TeamControler {

    @Autowired
    private TeamService TeamService;

    @GetMapping("/team")
    public List<Team> getAllteam() {
        return this.TeamService.findAllteam();
    }

    @PostMapping("/team")
    public Team createEquipe(@RequestBody Team team) {
        return this.TeamService.createTeam(team);
    }	
	
    @DeleteMapping("/team/{id}")
	public void deleteTeam(@PathVariable Long id) {
		
		this.TeamService.deleteTeam(id);
	} 
    
	@GetMapping("/team/{id}")
	public Team oneAnimal(@PathVariable Long id) {
				
		return this.TeamService.findTeam(id);
	}    

	@PutMapping("/team/{id}")
	Team replaceTeam(@RequestBody Team team, @PathVariable Long id) {

		Team foundTeam = this.TeamService.findTeam(id);
		
		if (team != null) {
			
			foundTeam.setName(team.getName());
			foundTeam.setCity(team.getCity());
			
			foundTeam = this.TeamService.saveTeam(foundTeam);
		}
		
		return foundTeam;
	}    
    
}
