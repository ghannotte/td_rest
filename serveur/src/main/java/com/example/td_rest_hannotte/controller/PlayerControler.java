package com.example.td_rest_hannotte.controller;


import com.example.td_rest_hannotte.domain.*;
import com.example.td_rest_hannotte.services.PlayerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
public class PlayerControler {


    @Autowired
    private PlayerService PlayerService;

    @GetMapping("/player")
    public List<Player> getAllPlayer() {
        return this.PlayerService.findAllPlayer();
    }

    @PostMapping("/player")
    public Player createEquipe(@RequestBody Player player) {
        return this.PlayerService.createPlayer(player);
    }	
	
    @DeleteMapping("/player/{id}")
	public void deletePlayer(@PathVariable Long id) {
		
		this.PlayerService.deletePlayer(id);
	} 
    
	@GetMapping("/player/{id}")
	public Player oneAnimal(@PathVariable Long id) {
				
		return this.PlayerService.findPlayer(id);
	}    

	@PutMapping("/player/{id}")
	Player replaceTeam(@RequestBody Player player, @PathVariable Long id) {

		Player foundPlayer = this.PlayerService.findPlayer(id);
		
		if (player != null) {
			
			foundPlayer.setName(player.getName());
			
			foundPlayer = this.PlayerService.savePlayer(foundPlayer);
		}
		
		return foundPlayer;
	}    
    
}	
	

