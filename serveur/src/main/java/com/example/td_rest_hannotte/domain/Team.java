package com.example.td_rest_hannotte.domain;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import com.example.td_rest_hannotte.domain.Player;
import java.util.*;

@Entity
public class Team {

    @Id
    @GeneratedValue
    public Long id;

    public String name;

    public String city;


	@OneToMany(mappedBy = "team", cascade = CascadeType.ALL)
    private List<Player> player; 
    
    public Team(){
    	
    }
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public String setName(String name) {
		return this.name=name;
	}

	public String setCity(String city) {
		return this.city=city;
	}
	
	public String getCity() {
		return city;
	}


}
