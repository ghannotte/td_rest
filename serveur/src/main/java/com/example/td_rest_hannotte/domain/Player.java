package com.example.td_rest_hannotte.domain;


import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import com.example.td_rest_hannotte.domain.*;
@Entity

@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String firstname;

    private String poste;
    

    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="team_name")
    private Team team;
    
    public Player(){
    	
    }
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}


	
	
	public String setName(String name) {
		return this.name=name;
	}	
	
	public String getfirstname() {
		return firstname;
	}
	
	public String setfirstname(String firstname) {
		return this.firstname=firstname;
	}

	public String setposte(String poste) {
		return this.poste=poste;
	}
	public String getposte() {
		return poste;
	}
	
}
